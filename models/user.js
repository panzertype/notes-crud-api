const mongoose = require('mongoose');

const userSchema = new mongoose.Schema(
    {
      username: {
        type: String,
        required: true,
        unique: true,
      },
      hash: {
        type: String,
        required: true,
      },
    },
    {
      timestamps: {createdAt: 'createdDate', updatedAt: false},
    },
);

module.exports = mongoose.model('User', userSchema);
