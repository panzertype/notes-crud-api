const mongoose = require('mongoose');

const noteSchema = new mongoose.Schema(
    {
      userId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'User',
      },
      completed: {
        type: Boolean,
        required: true,
      },
      text: {
        type: String,
        required: true,
      },
    },
    {
      timestamps: {createdAt: 'createdDate', updatedAt: false},
    },
);

module.exports = mongoose.model('Note', noteSchema);
