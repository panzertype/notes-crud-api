const User = require('../models/user');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

const register = async (req, res) => {
  const {password, username} = req.body;
  try {
    if (!password) {
      console.log('ERROR 400: Please specify \'password\' parameter');
      res.status(400).send({
        message: 'Please specify \'password\' parameter',
      });
      return;
    } else if (!username) {
      console.log('ERROR 400: Please specify \'content\' parameter');
      res.status(400).send({
        message: 'Please specify \'username\' parameter',
      });
      return;
    }

    const hash = await bcrypt.hash(password, 10);
    const user = new User({
      username,
      hash,
    });
    await user.save();
    res.send({message: 'Success'});
  } catch (err) {
    console.log(err);
    res.status(500).send({message: err});
  }
};

const login = async (req, res) => {
  const {password, username} = req.body;
  try {
    if (!password) {
      console.log('ERROR 400: Please specify \'password\' parameter');
      res.status(400).send({
        message: 'Please specify \'password\' parameter',
      });
      return;
    } else if (!username) {
      console.log('ERROR 400: Please specify \'content\' parameter');
      res.status(400).send({
        message: 'Please specify \'username\' parameter',
      });
      return;
    }
    const user = await User.findOne({username});
    const response = await bcrypt.compare(password, user.hash);
    if (response) {
      res.send({message: 'Success', jwt_token: genToken(user._id)});
    } else {
      res.status(400).send({message: 'Invalid username or password'});
    }
  } catch (err) {
    console.log(err);
    res.status(500).send({message: err});
  }
};

// Generate JWT
const genToken = (id) => {
  return jwt.sign({id}, process.env.JWT_SECRET, {
    expiresIn: '1d',
  });
};

module.exports = {
  register,
  login,
};
