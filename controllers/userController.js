const User = require('../models/user');

const getInfo = async (req, res) => {
  const user = await User.findById(req.user._id).select('-__v -hash');

  if (user) {
    res.status(200).json({user});
  } else {
    console.log('ERROR 400: User doesn\'t exist');
    res.status(400).send({
      message: 'User doesn\'t exist',
    });
  }
};

const deleteUser = async (req, res) => {
  const user = await User.findById(req.user._id);
  if (user) {
    await user.remove();
    res.send({message: 'Success'});
  } else {
    console.log('ERROR 400: User doesn\'t exist');
    res.status(400).send({
      message: 'User doesn\'t exist',
    });
  }
};

module.exports = {
  getInfo,
  deleteUser,
};
