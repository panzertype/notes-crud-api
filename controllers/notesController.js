const Note = require('../models/note');

const getNotes = async (req, res) => {
  try {
    const urlString = req.protocol + '://' + req.hostname + req.originalUrl;
    const url = new URL(urlString);
    const offset = +url.searchParams.get('offset');
    const limit = +url.searchParams.get('limit');

    const notes = await Note.find({userId: req.user._id})
        .skip(offset)
        .limit(limit)
        .select('-__v');

    res.status(200).json({offset, limit, count: notes.length, notes});
  } catch (err) {
    if (err.message) {
      console.log(err);
      res.status(500).send({message: err.message});
    } else {
      console.log(err);
      res.status(500).send({message: err});
    }
  }
};

const addNote = async (req, res) => {
  try {
    await Note.create({
      text: req.body.text,
      completed: false,
      userId: req.user.id,
    });
    res.send({message: 'Success'});
  } catch (err) {
    if (err.message) {
      console.log(err);
      res.status(500).send({message: err.message});
    } else {
      console.log(err);
      res.status(500).send({message: err});
    }
  }
};

const getNote = async (req, res) => {
  try {
    const note = await Note.findOne({
      _id: req.params.id,
      userId: req.user.id,
    }).select('-__v');
    if (note) {
      res.status(200).json({note});
    } else {
      console.log('ERROR 400: Note doesn\'t exist');
      res.status(400).send({
        message: 'Note doesn\'t exist',
      });
    }
  } catch (err) {
    if (err.message) {
      console.log(err);
      res.status(500).send({message: err.message});
    } else {
      console.log(err);
      res.status(500).send({message: err});
    }
  }
};

const updateNote = async (req, res) => {
  try {
    const note = await Note.findOne({_id: req.params.id, userId: req.user.id});
    if (note) {
      note.text = req.body.text;
      await note.save();
      res.status(200).send({message: 'Success'});
    } else {
      console.log('ERROR 400: Note doesn\'t exist');
      res.status(400).send({
        message: 'Note doesn\'t exist',
      });
    }
  } catch (err) {
    if (err.message) {
      console.log(err);
      res.status(500).send({message: err.message});
    } else {
      console.log(err);
      res.status(500).send({message: err});
    }
  }
};

const checkNote = async (req, res) => {
  try {
    const note = await Note.findOne({
      _id: req.params.id,
      userId: req.user.id,
    });
    if (note) {
      note.completed = !note.completed;
      await note.save();
      res.status(200).send({message: 'Success'});
    } else {
      console.log('ERROR 400: Note doesn\'t exist');
      res.status(400).send({
        message: 'Note doesn\'t exist',
      });
    }
  } catch (err) {
    if (err.message) {
      console.log(err);
      res.status(500).send({message: err.message});
    } else {
      console.log(err);
      res.status(500).send({message: err});
    }
  }
};

const deleteNote = async (req, res) => {
  try {
    const note = await Note.findOne({
      _id: req.params.id,
      userId: req.user.id,
    });
    if (note) {
      await note.remove();
      res.send({message: 'Success'});
    } else {
      console.log('ERROR 400: Note doesn\'t exist');
      res.status(400).send({
        message: 'Note doesn\'t exist',
      });
    }
  } catch (err) {
    if (err.message) {
      console.log(err);
      res.status(500).send({message: err.message});
    } else {
      console.log(err);
      res.status(500).send({message: err});
    }
  }
};

module.exports = {
  getNotes,
  addNote,
  getNote,
  updateNote,
  checkNote,
  deleteNote,
};
