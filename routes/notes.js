const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const {
  addNote,
  getNotes,
  getNote,
  updateNote,
  checkNote,
  deleteNote,
} = require('../controllers/notesController');
const {protect} = require('../middleware/authMiddleware');

router.get('/', protect, getNotes);
router.post('/', protect, addNote);
router.get('/:id', protect, getNote);
router.put('/:id', protect, updateNote);
router.patch('/:id', protect, checkNote);
router.delete('/:id', protect, deleteNote);

module.exports = router;
