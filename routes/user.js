const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const {
  getInfo,
  deleteUser,
} = require('../controllers/userController');
const {protect} = require('../middleware/authMiddleware');

router.get('/', protect, getInfo);
router.delete('/', protect, deleteUser);

module.exports = router;
