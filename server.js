require('dotenv').config();
const cors = require('cors');
const express = require('express');
const connectDB = require('./config/db');

connectDB();

const app = express();

const userRouter = require('./routes/user');
const authRouter = require('./routes/auth');
const notesRouter = require('./routes/notes');

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use('/api/users/me', userRouter);
app.use('/api', authRouter);
app.use('/api/notes', notesRouter);

app.listen(process.env.PORT || 8080);
