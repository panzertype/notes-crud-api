const jwt = require('jsonwebtoken');
const User = require('../models/user');

const protect = async (req, res, next) => {
  let token;

  if (
    req.headers.authorization
  ) {
    try {
      if (req.headers.authorization.startsWith('JWT')) {
        token = req.headers.authorization.split(' ')[1];
      } else {
        token = req.headers.authorization;
      }

      const decoded = jwt.verify(token, process.env.JWT_SECRET);

      req.user = await User.findById(decoded.id).select('-hash');

      next();
    } catch (err) {
      console.log(err);
      res.status(400).send({
        message: 'Token is expired or invalid',
      });
    }
  }

  if (!token) {
    console.log('ERROR 400: Please specify \'token\' parameter');
    res.status(400).send({
      message: 'Please specify \'token\' parameter',
    });
  }
};

module.exports = {protect};
